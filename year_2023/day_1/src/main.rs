use std::collections::HashMap;
use std::fs::read_to_string;
use regex::Regex;


fn main() {
    let sum_part_1 = calculate_sum_part_1("src/input_real.txt");
    println!("Part 1: {sum_part_1}");
    let sum_part_2 = calculate_sum_part_2("src/input_real.txt");
    println!("Part 2: {sum_part_2}");
}

fn get_matches(input_path: &str, regex: &str) -> Vec<Vec<String>> {
    let re = Regex::new(regex).unwrap();
    read_to_string(input_path).unwrap()
        .lines()
        .map(|line| re
            .find_iter(line)
            .map(|m| String::from(m.as_str()))
            .collect())
        .collect()
}

fn calculate_sum(matches: Vec<Vec<String>>) -> u32 {
    matches
        .iter()
        .map(|lm| (lm[0].to_string() + lm.last().unwrap()))
        .map(|str_num| str_num.parse::<u32>().unwrap())
        .reduce(|acc, e| acc + e)
        .unwrap()
}

fn matches_to_num_str(matches: Vec<Vec<String>>, lookup: HashMap<String, String>) -> Vec<Vec<String>> {
    matches
        .iter()
        .map(|lm| lm
            .iter()
            .map(|m| lookup.get(m).unwrap().to_string())
            .collect()
        )
        .collect()
}

fn calculate_sum_part_1(input_path: &str) -> u32 {
    let matches = get_matches(input_path, r"\d");
    calculate_sum(matches)
}
fn calculate_sum_part_2(input_path: &str) -> u32 {
    const NUMBERS: [&str; 9] = [
        "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"
    ];
    let mut numbers_lookup = HashMap::new();
    for (i, number) in NUMBERS.iter().enumerate() {
        let ip1 = i+1;
        numbers_lookup.insert(number.to_string(), ip1.to_string());
        numbers_lookup.insert(ip1.to_string(), ip1.to_string());
    }
    let capture_number_str = NUMBERS.join("|");
    let re = format!(r"\d|{capture_number_str}");

    let matches = get_matches(input_path, &re);
    let fixed_matches = matches_to_num_str(matches, numbers_lookup);

    // let lines: Vec<&str> = 
    for (i, line) in read_to_string(input_path).unwrap().lines().enumerate() {
        let number = fixed_matches[i].get(0).unwrap().to_string() + fixed_matches[i].last().unwrap();
        println!("{i}: {line} -> {number}");
    }
    calculate_sum(fixed_matches)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_calculate_sum_part_1() {
        let input_path = "src/input_test_part_1.txt";
        assert_eq!(calculate_sum_part_1(input_path), 142);
    }
    #[test]
    fn test_calculate_sum_part_2() {
        let input_path = "src/input_test_part_2.txt";
        assert_eq!(calculate_sum_part_2(input_path), 281);
    }
}