extern crate core;

use std::fs::read_to_string;

fn main() {
    let result = part_1("src/input_real.txt");
    println!("Part 1: {result}");
}


#[derive(PartialEq)]
#[derive(Debug)]
struct Number {
    line: i32,
    start: i32,
    end: i32,
    value: i32,
}
impl Number {
    fn parse(line_num: i32, raw: &str) -> Vec<Self> {
        let mut start: Option<i32> = None;
        let mut finish: Option<i32> = None;
        let mut num_value: String = "".to_string();
        let mut numbers: Vec<Number> = Vec::new();
        for (position, c) in raw.chars().enumerate() {
            if c.is_digit(10) {
                num_value += &c.to_string().as_str();
            }
            if start.is_none() && c.is_digit(10) {
                start = Some(position as i32);
            }
            if finish.is_none() && start.is_some() && (!c.is_digit(10)) {
                finish = Some(position as i32);
            }
            if start.is_some() && finish.is_some() {
                numbers.push(Number {
                    line: line_num,
                    start: start.unwrap(),
                    end: finish.unwrap(),
                    value: num_value.parse().unwrap(),
                });
                start = None;
                finish = None;
                num_value = "".to_string();
            }
        }
        numbers
    }
    fn check(&self, matrix: &Vec<&str>) -> bool {
        let num_cols = matrix.get(0).unwrap().len() as i32;
        let num_rows = matrix.len() as i32;
        for row in (self.line - 1)..(self.line + 2) {
            for col in (self.start - 1)..(self.end + 1) {
                // discard the number itself
                let overlaps_number = row == self.line && col >= self.start && col < self.end;
                let out_of_matrix = row < 0 || row >= num_rows || col < 0 || col >= num_cols;
                if overlaps_number || out_of_matrix {
                    continue;
                }
                let curr_char = matrix.get(row as usize).unwrap().chars().nth(col as usize).unwrap();
                if curr_char != '.' {
                    return true;
                }
            }
        }
        false
    }
}


fn part_1(input_path: &str) -> i32 {
    let content = read_to_string(input_path).unwrap();
    let matrix: Vec<_> = content.lines().collect();
    matrix.iter()
        .enumerate()
        .flat_map(|(line_num, line)| Number::parse(line_num as i32, line))
        .filter(|n| n.check(&matrix))
        .fold(0, |acc, e| acc + e.value)
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_1() {
        assert_eq!(part_1("src/input_test.txt"), 4361);
    }

    #[test]
    fn test_parse_numbers() {
        let expected = vec![
            Number { line: 0, start: 0, end: 3, value: 467 },
            Number { line: 0, start: 5, end: 8, value: 114 }
        ];
        assert_eq!(Number::parse(0, "467..114..", ), expected);
    }

    #[test]
    fn test_check_numbers() {
        let number = Number { line: 0, start: 0, end: 3, value: 467 };
        assert!(number.check(&vec!["467*.114.."]));
        assert!(number.check(&vec![
            "467..114..",
            "..*......."
        ]));
        assert!(number.check(&vec![
            "..*.......",
            "467..114..",
        ]));
        assert!(!number.check(&vec!["467..114*."]));
    }
    // #[test]
    // fn test_part_2() {
    //     let input_path = "src/input_test.txt";
    //     assert_eq!(part_2(input_path), 2286);
    // }
}