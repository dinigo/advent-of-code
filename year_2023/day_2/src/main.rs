use std::fs::read_to_string;

fn main() {
    let score_1 = part_1("src/input_real.txt");
    println!("Part 1: {score_1}");
    let score_2 = part_2("src/input_real.txt");
    println!("Part 2: {score_2}");
}

fn part_1(input_path: &str) -> u32 {
    let content = read_to_string(input_path).unwrap();
    let games = content.lines().map(Game::parse);
    let constrains = Set { r: 12, g: 13, b: 14 };
    games
        .filter(|g| g.check(&constrains))
        .fold(0, |acc, g| acc + g.id)
}

fn part_2(input_path: &str) -> u32 {
    let content = read_to_string(input_path).unwrap();
    let games = content.lines().map(Game::parse);
    games
        .map(|g| g.cubes_power())
        .reduce(|acc, e| acc + e)
        .unwrap()
}


#[derive(PartialEq)]
#[derive(Debug)]
struct Game {
    id: u32,
    sets: Vec<Set>,
}

#[derive(Default)]
#[derive(Debug)]
#[derive(PartialEq)]
#[derive(Clone)]
struct Set {
    r: u32,
    g: u32,
    b: u32,
}

impl Set {
    fn parse(raw: &str) -> Self {
        let mut my_set = Set::default();
        raw.split(", ")
            .map(|e| e.split(" ").collect::<Vec<&str>>())
            .for_each(|c| my_set.set(c[1], c[0].parse().unwrap()));
        my_set
    }
    fn set(&mut self, color: &str, value: u32) {
        match color {
            "red" => self.r = value,
            "green" => self.g = value,
            "blue" => self.b = value,
            _ => panic!("Color not supported: {color}"),
        }
    }
}

impl Game {
    fn parse(raw_line: &str) -> Self {
        let line = &raw_line[5..raw_line.len()];
        let parts: Vec<_> = line.split(": ").collect();
        let my_sets: Vec<_> = parts[1].split("; ").map(|s| Set::parse(s)).collect();
        Game { id: parts[0].parse().unwrap(), sets: my_sets }
    }
    fn check(&self, constrains: &Set) -> bool {
        !self.sets.iter().any(|s| s.r > constrains.r || s.g > constrains.g || s.b > constrains.b)
    }
    fn cubes_power(&self) -> u32 {
        let max_set = self.sets.iter().fold(Set::default(), |mut acc, s| {
            if s.r > acc.r {
                acc.r = s.r;
            }
            if s.g > acc.g {
                acc.g = s.g;
            }
            if s.b > acc.b {
                acc.b = s.b;
            }
            acc
        });
        max_set.r * max_set.g * max_set.b
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_set() {
        let input = "3 blue, 4 red";
        assert_eq!(Set::parse("3 blue, 4 red"), Set { r: 4, g: 0, b: 3 })
    }

    #[test]
    fn test_set_set() {
        let expected = Set { r: 1, g: 0, b: 0 };
        let mut my_set = Set::default();
        my_set.set("red", 1);
        assert_eq!(my_set, expected);
    }

    #[test]
    fn test_parse_game() {
        let line = "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green";
        let expected = Game { id: 1, sets: vec![Set { b: 3, r: 4, g: 0 }, Set { r: 1, g: 2, b: 6 }, Set { g: 2, r: 0, b: 0 }] };
        assert_eq!(Game::parse(line), expected);
    }
    #[test]
    fn test_part_1() {
        assert_eq!(part_1("src/input_test.txt"), 8);
    }
    #[test]
    fn test_part_2() {
        let input_path = "src/input_test.txt";
        assert_eq!(part_2(input_path), 2286);
    }
}