#!/usr/bin/env node

const fs = require('fs');

const strategy = fs.readFileSync('input.txt', 'utf8');

const actions = {A: 'rock', B: 'paper', C: 'scissors'};
const responses = {X: 'rock', Y: 'paper', Z: 'scissors'};
const expected = {X: 'loose', Y: 'draw', Z: 'win'};
const idx = {rock: 0, paper: 1, scissors: 2};
const idx_expected = {loose: 0, draw: 1, win: 2};
const win = 6, draw = 3, loose = 0;
const play_score = [
    [draw, loose, win],
    [win, draw, loose],
    [loose, win, draw]
];
const rock = 1, paper = 2, scissors = 3;
const play_move = [
    [scissors, rock, paper],
    [rock, paper, scissors],
    [paper, scissors, rock]
];

function part2(acum, [act,exp]) {
    const act_val = actions[act]
    const exp_val = expected[exp]
    const act_idx = idx[act_val]
    const exp_idx = idx_expected[exp_val]
    const round_score = exp_idx * 3
    const shape_score = play_move[act_idx][exp_idx]
    return acum + shape_score + round_score
}

function part1(acum, [act,res]) {
    const act_val = actions[act]
    const res_val = responses[res]
    const act_idx = idx[act_val]
    const res_idx = idx[res_val]
    const shape_score = res_idx + 1
    const round_score = play_score[res_idx][act_idx]
    return acum + shape_score + round_score
}

const score = (strategy, reducer) => strategy
    .split('\n').map(l => l.split(' '))
    .reduce(reducer, 0);

const score_part1 = score(strategy, part1)
const score_part2 = score(strategy, part2)

console.log({score_part1, score_part2});