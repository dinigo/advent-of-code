#!/usr/bin/env node

const fs = require('fs');

const numbers_str = fs.readFileSync('input.txt', 'utf8');

const calories_per_elve = numbers_str
    .split('\n\n')
    .map(elve => elve.split('\n')
        .map(k => +k)
        .reduce((a, c) => a + c))
    .sort((a, b) => b - a);

const part1 = calories_per_elve[0];
const part2 = calories_per_elve.slice(0, 3).reduce((a, c) => a + c);
console.log({part1, part2});