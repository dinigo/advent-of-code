#!/usr/bin/env run-cargo-script

use std::fs;

fn main() {
    let file_path = "input.txt";
    println!("Reading file: {}", file_path);
    // Read the input.txt file
    let contents = fs::read_to_string(file_path)
        .expect("Something went wrong reading the file");
    let mut total_calories = extract_cals(contents);

    total_calories.sort_by(|a, b| b.cmp(a));
    let part_1 = total_calories[0];
    let part_2 = total_calories[0] + total_calories[1] + total_calories[2];
    println!("Max calories carried by elf: {:?}", part_1);
    println!("Max calories carried by elf: {:?}", part_2);
}

fn extract_cals(contents: String) -> Vec<i32> {
    let blocks = contents.split("\n\n");
    let mut total_calories = Vec::new();
    for block in blocks {
        let calories = block.split("\n");
        let mut elf_total_calories = 0;
        for caloric in calories {
            elf_total_calories += caloric.parse::<i32>().unwrap();
        }
        total_calories.push(elf_total_calories);
    }
    total_calories
}

#[test]
fn test_extract_cals() {
    let contents = "100\n200\n\n400";
    let expected = vec![300, 400];
    let actual = extract_cals(contents.to_string());
    assert_eq!(expected, actual);
}