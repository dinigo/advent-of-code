// /usr/bin/true; exec /usr/bin/env go run "$0" "$@"
package main

import (
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
)

func main() {
	input_bytes, _ := os.ReadFile("input.txt")
	input_str := string(input_bytes)
	//split string each new line
	elves := strings.Split(input_str, "\n\n")
	// make new integer array the same length as lines
	calories_per_elf := make([]int, len(elves))
	for i, elf := range elves {
		// split each line into an array of strings
		calories := strings.Split(elf, "\n")
		// cast the calories array to int and summ it
		for _, calorie := range calories {
			calorie_int, _ := strconv.Atoi(calorie)
			calories_per_elf[i] += calorie_int
		}
	}
	// sort calories_per_elf array descending
	sort.Sort(sort.Reverse(sort.IntSlice(calories_per_elf)))
	// print the first 3 elements
	fmt.Println("part 1:", calories_per_elf[0])
	fmt.Println("part 2:", calories_per_elf[0]+calories_per_elf[1]+calories_per_elf[2])
}
