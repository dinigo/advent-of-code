package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func GetFish(path string) []int {
	// read the file
	content, _ := ioutil.ReadFile(path)
	fishDaysStr := strings.Split(string(content), ",")
	fishDays := make([]int, len(fishDaysStr))
	for i, fd := range fishDaysStr {
		fisDaysInt, _ := strconv.Atoi(fd)
		fishDays[i] = fisDaysInt
	}
	return fishDays
}

func CalculateBreedDays(currentFishCycle int, remainingDays int) int {
	if remainingDays == 0 {
		return 1
	}
	if currentFishCycle == 0 {
		age := CalculateBreedDays(6, remainingDays-1)
		breed := CalculateBreedDays(8, remainingDays-1)
		return age + breed
	} else {
		return CalculateBreedDays(currentFishCycle-1, remainingDays-1)
	}
}

type CalculateBreedDaysParams struct {
	currentFishCycle int
	remainingDays    int
}

func CalculateBreedDaysMemoized(currentFishCycle int, remainingDays int, cache map[CalculateBreedDaysParams]int) int {
	// try to retrieve from cache
	cacheKey := CalculateBreedDaysParams{remainingDays: remainingDays, currentFishCycle: currentFishCycle}
	if val, found := cache[cacheKey]; found {
		return val
	}

	// fallback to regular function if not cached
	var result int
	if remainingDays == 0 {
		result = 1
	} else if currentFishCycle == 0 {
		age := CalculateBreedDaysMemoized(6, remainingDays-1, cache)
		breed := CalculateBreedDaysMemoized(8, remainingDays-1, cache)
		result = age + breed
	} else {
		result = CalculateBreedDaysMemoized(currentFishCycle-1, remainingDays-1, cache)
	}

	// store in cache prior to return
	cache[cacheKey] = result
	return result
}

func main() {
	// new lantern fish every week
	// lantern_fish(days_before_breed)
	// delay after breed
	fishDays := GetFish("2021/day_6/day_6_input.txt")
	sum := 0
	for _, age := range fishDays {
		sum += CalculateBreedDays(age, 80)
	}
	fmt.Printf("day 6.1: %v\n", sum)

	sum = 0
	// maps are always passed by reference
	cache := make(map[CalculateBreedDaysParams]int)
	for _, age := range fishDays {
		sum += CalculateBreedDaysMemoized(age, 256, cache)
	}
	fmt.Printf("day 6.2: %v\n", sum)
}
