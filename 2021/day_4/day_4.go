package main

import (
	"fmt"
	"io/ioutil"
	"regexp"
	"strconv"
	"strings"
)

const NO_RESULT = -1

func GetBoardsAndBalls(path string) ([][][]int, []int) {
	// read the file
	content, _ := ioutil.ReadFile(path)
	elements := strings.Split(string(content), "\n\n")

	// get the balls
	ballsString := strings.Split(elements[0], ",")
	balls := make([]int, len(ballsString))
	for i, v := range ballsString {
		ballNumber, _ := strconv.Atoi(v)
		balls[i] = ballNumber
	}

	// get the boardsString
	boardsString := elements[1:]
	boards := make([][][]int, len(boardsString))
	for i, board := range boardsString {
		boardLines := strings.Split(board, "\n")
		boards[i] = make([][]int, len(boardLines))
		for k, line := range boardLines {
			lineNumsStr := regexp.MustCompile(`\d+`).FindAllString(line, -1)
			boards[i][k] = make([]int, len(lineNumsStr))
			for l, numStr := range lineNumsStr {
				ballNumber, _ := strconv.Atoi(numStr)
				boards[i][k][l] = ballNumber
			}
		}
	}
	return boards, balls
}

func BoardToRowsAndCols(board [][]int) [][]int {
	// get transposed
	transposed := make([][]int, len(board))
	for i := range board {
		transposed[i] = make([]int, len(board))
	}
	for i, line := range board {
		for j, cell := range line {
			transposed[j][i] = cell
		}
	}
	// append rows and cols to easily filter
	rowsAndCols := make([][]int, len(board)*2)
	for i := range board {
		rowsAndCols[i] = board[i]
		rowsAndCols[len(board)+i] = transposed[i]
	}
	return rowsAndCols
}

func ApplyBall(boardRowsCols [][]int, ball int) int {
	// remember that this board contains twice as many rows, cause it contains also it's transposed
	for i, line := range boardRowsCols {
		// calculate the new filtered line
		tmpLine := make([]int, 0)
		for _, num := range line {
			if num != ball {
				tmpLine = append(tmpLine, num)
			}
		}
		// replace the line for the old one
		boardRowsCols[i] = tmpLine

		// if one of the rows or cols went 0
		if len(tmpLine) == 0 {
			sum := 0
			for i := 0; i < len(boardRowsCols)/2; i++ {
				for _, n := range boardRowsCols[i] {
					sum += n
				}
			}
			return sum * ball
		}
	}
	return NO_RESULT
}

func CalculateSolutions() []int {
	boards, balls := GetBoardsAndBalls("2021/day_4/day_4_input.txt")
	for i, board := range boards {
		boards[i] = BoardToRowsAndCols(board)
	}
	var solutions []int
	for _, ball := range balls {
		i := 0
		for i < len(boards) {
			solution := ApplyBall(boards[i], ball)
			if solution != NO_RESULT {
				solutions = append(solutions, solution)
				// if this board got a result then remove it

				boards = append(boards[:i], boards[i+1:]...)
			} else {
				i++
			}
		}
	}
	return solutions
}

func main() {
	solutions := CalculateSolutions()
	fmt.Printf("day 4.1: %v\n", solutions[0])
	fmt.Printf("day 4.2: %v\n", solutions[len(solutions)-1])
}
