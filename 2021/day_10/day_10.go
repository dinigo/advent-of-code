package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"sort"
	"strings"
)

func GetCode(path string) [][]string {
	content, _ := ioutil.ReadFile(path)
	lines := strings.Split(string(content), "\n")
	code := make([][]string, len(lines))
	for i, line := range lines {
		code[i] = strings.Split(line, "")
	}
	return code
}

func Main1() {
	code := GetCode("2021/day_10/day_10_input.txt")
	// find the low points
	bracket := map[string]string{
		")": "(",
		"}": "{",
		"]": "[",
		">": "<",
	}
	points := map[string]int{
		")": 3,
		"]": 57,
		"}": 1197,
		">": 25137,
	}
	score := 0
	for _, line := range code {
		var stack = [100]string{}
		sc := -1 // stack counter
		for _, char := range line {
			if char == "(" || char == "[" || char == "<" || char == "{" {
				sc++
				stack[sc] = char
			} else if closeChar, _ := bracket[char]; sc > -1 && closeChar == stack[sc] {
				sc--
			} else {
				score += points[char]
				break
			}
		}
	}
	fmt.Printf("day 10.1: %v\n", score)
}

func Main2() {
	code := GetCode("2021/day_10/day_10_input.txt")
	// find the low points
	bracket := map[string]string{
		")": "(",
		"}": "{",
		"]": "[",
		">": "<",
	}
	points := map[string]int{
		"(": 1,
		"[": 2,
		"{": 3,
		"<": 4,
	}
	scores := make([]int, 0)

	for _, line := range code {
		score := 0
		valid := true
		var stack = [100]string{}
		sc := -1 // stack counter
		for _, char := range line {
			if char == "(" || char == "[" || char == "<" || char == "{" {
				sc++
				stack[sc] = char
			} else if closeChar, _ := bracket[char]; sc > -1 && closeChar == stack[sc] {
				sc--
			} else {
				valid = false
				break
			}
		}
		if valid {
			for sc >= 0 {
				score = score*5 + points[stack[sc]]
				sc--
			}
			scores = append(scores, score)
		}
	}

	sort.Ints(scores)
	middleIndex := int(math.Floor(float64(len(scores) / 2)))
	fmt.Printf("day 10.2: %v\n", scores[middleIndex])
}

func main() {
	Main1()
	Main2()
}
