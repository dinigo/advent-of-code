package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"regexp"
	"strconv"
	"strings"
)

func GetVents(path string, includeDiagonal bool) [][]int {
	// read the file
	content, _ := ioutil.ReadFile(path)
	ventLines := strings.Split(string(content), "\n")

	// get all coordinates
	var vents [][]int
	for _, line := range ventLines {
		coordinatesStr := regexp.MustCompile(`\d+`).FindAllString(line, -1)
		coordinates := make([]int, len(coordinatesStr))
		for k, cordStr := range coordinatesStr {
			cord, _ := strconv.Atoi(cordStr)
			coordinates[k] = cord
		}
		// filter only horizontal and vertical lines of vents
		isHorizontalOrVertical := (coordinates[0] == coordinates[2]) || (coordinates[1] == coordinates[3])

		if isHorizontalOrVertical || includeDiagonal {
			vents = append(vents, coordinates)
		}
	}
	return vents
}

func GetIncrement(a int, b int) int {
	if a == b {
		return 0
	} else if a > b {
		return -1
	} else {
		return 1
	}
}

func DrawVentsOnMap(vents [][]int) [][]int {
	// get canvas dimensions
	maxX, maxY := 0, 0
	for _, vent := range vents {
		biggestX := int(math.Max(float64(vent[1]), float64(vent[3])))
		if biggestX > maxX {
			maxX = biggestX
		}
		biggestY := int(math.Max(float64(vent[0]), float64(vent[2])))
		if biggestY > maxY {
			maxY = biggestY
		}
	}
	//create map
	ventsMap := make([][]int, maxX+1)
	for i := range ventsMap {
		ventsMap[i] = make([]int, maxY+1)
	}
	// fill the map
	for _, vent := range vents {
		// calculate increments
		incX := GetIncrement(vent[1], vent[3])
		incY := GetIncrement(vent[0], vent[2])
		for x, y := vent[1], vent[0]; x != vent[3]+incX || y != vent[2]+incY; x, y = x+incX, y+incY {
			ventsMap[x][y]++
		}
	}
	return ventsMap
}

func GetOverlaps(ventsMap [][]int) int {
	overlaps := 0
	for _, line := range ventsMap {
		for _, cell := range line {
			if cell > 1 {
				overlaps++
			}
		}
	}
	return overlaps
}

func DisplayMap(ventsMap [][]int, maxSize int) {
	if len(ventsMap) < maxSize {
		for _, line := range ventsMap {
			fmt.Println(strings.Replace(strings.Replace(fmt.Sprint(line), " ", "", -1), "0", ".", -1))
		}
	}
}

func Main1() {
	vents := GetVents("2021/day_5/day_5_input.txt", false)
	ventsMap := DrawVentsOnMap(vents)
	DisplayMap(ventsMap, 80)
	overlaps := GetOverlaps(ventsMap)
	fmt.Printf("day 5.1: %v\n", overlaps)
}

func Main2() {
	vents := GetVents("2021/day_5/day_5_input.txt", true)
	ventsMap := DrawVentsOnMap(vents)
	DisplayMap(ventsMap, 80)
	overlaps := GetOverlaps(ventsMap)
	fmt.Printf("day 5.2: %v\n", overlaps)
}

func main() {
	Main1()
	Main2()
}
