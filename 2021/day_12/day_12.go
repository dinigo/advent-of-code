package main

import (
	"fmt"
	"io/ioutil"
	"strings"
)

func main() {
	input, _ := ioutil.ReadFile("2021/day_12/day_12_input.txt")
	inputString := string(input)
	// build an incidence matrix with the pairs of nodes in the lines separated by "-"
	adjacencyList := make(map[string][]string)
	for _, line := range strings.Split(inputString, "\n") {
		nodes := strings.Split(line, "-")
		adjacencyList[nodes[0]] = append(adjacencyList[nodes[0]], nodes[1])
		adjacencyList[nodes[1]] = append(adjacencyList[nodes[1]], nodes[0])
	}
	pathsWithSmallCaves := explore(adjacencyList, "start", []string{}, map[string]int{"start": 1}, CaveNotVisited)
	fmt.Printf("day 12.1: %v\n", pathsWithSmallCaves)
	pathsWithSmallCavesTwice := explore(adjacencyList, "start", []string{}, map[string]int{}, NoCaveVisitedMoreThanOnce)
	fmt.Printf("day 12.2: %v\n", pathsWithSmallCavesTwice)

}

func explore(adjacencyList map[string][]string, current string, path []string, visited map[string]int, policy func(map[string]int, string) bool) int {
	// if we have reached the end node, we have found a path
	path = append(path, current)
	if current == "end" {
		return 1
	}

	pathsWithSingleSmallCave := 0
	// if we have not reached the end node, we must explore all the possible paths starting from the current node that are not yet visited
	for _, node := range adjacencyList[current] {
		isBigCave := strings.ToUpper(node) == node
		isValidSmallCave := policy(visited, node)
		isNotStart := node != "start"
		if isNotStart && (isValidSmallCave || isBigCave) {
			// copy visited into new map, otherwise will be shared between all the explorations
			visitedCopy := make(map[string]int)
			for k, v := range visited {
				visitedCopy[k] = v
			}
			// only set as visited if it is a small cave
			if !isBigCave {
				visitedCopy[node]++
			}
			pathsWithSingleSmallCave += explore(adjacencyList, node, path, visitedCopy, policy)
		}
	}
	return pathsWithSingleSmallCave
}

func CaveNotVisited(visited map[string]int, node string) bool {
	_, ok := visited[node]
	return !ok
}

func NoCaveVisitedMoreThanOnce(visited map[string]int, node string) bool {
	visitedCount, _ := visited[node]

	switch {
	case node == "start" || node == "end":
		// start and end nodes can only be repeated once.
		return visitedCount == 0
	case strings.ToLower(node) == node:
		if visitedCount == 0 {
			// if small cave and not visited then can be
			return true
		} else if visitedCount == 1 {
			// elif small cave and visited once can be visited again if it's the first to go over 1
			for otherNode, count := range visited {
				if node != otherNode && count > 1 {
					return false
				}
			}
			return true
		} else {
			// for the rest of lengths (>1) we cannot visit anymore this small cave
			return false
		}
	default:
		// other cases are big caves, than can always be repeated
		return true
	}
}
