package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

func CalculateDiffs(depths []int) int {
	diffs := 0
	for i := 0; i < len(depths)-1; i++ {
		if depths[i+1]-depths[i] > 0 {
			diffs++
		}
	}
	return diffs
}
func TextToNumbers(text string) []int {
	lines := strings.Split(text, "\n")
	result := make([]int, len(lines))
	for i, v := range lines {
		num, _ := strconv.Atoi(v)
		result[i] = num
	}
	return result
}
func ReadNumsFile(filePath string) []int {
	dat, _ := os.ReadFile(filePath)
	depthsText := string(dat)
	depthNumbers := TextToNumbers(depthsText)
	return depthNumbers
}
func Main1() {
	depthNumbers := ReadNumsFile("2021/day_1/day_1_input.txt")
	got := CalculateDiffs(depthNumbers)
	fmt.Printf("day 1.1: %d\n", got)
}
func CalculateWindows(depths []int, windowWidth int) []int {
	windowSumList := make([]int, len(depths)-windowWidth+1)
	// calculate each window sum
	for i := range windowSumList {
		window := depths[i : i+windowWidth]
		sum := 0
		for _, num := range window {
			sum += num
		}
		windowSumList[i] = sum
	}
	return windowSumList
}
func Main2() {
	depthNumbers := ReadNumsFile("2021/day_1/day_1_input.txt")
	depthWindows := CalculateWindows(depthNumbers, 3)
	got := CalculateDiffs(depthWindows)
	fmt.Printf("day 1.2 %d\n", got)
}
func main() {
	Main1()
	Main2()
}
