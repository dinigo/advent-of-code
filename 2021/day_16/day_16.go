package main

import (
	"fmt"
	"strconv"
)

const TYPE_ID_SUM = 0
const TYPE_ID_PROD = 1
const TYPE_ID_MIN = 2
const TYPE_ID_MAX = 3
const TYPE_ID_LITERAL = 4
const TYPE_ID_GREATER = 5
const TYPE_ID_LESS = 6
const TYPE_ID_EQUAL = 7

const OPERATOR_LENGTH_BITS = 0
const OPERATOR_LENGTH_NUM = 1

var OPERATORS_MAP = map[int]func(...int) int{
	TYPE_ID_SUM: func(nums ...int) int {
		// return the sum of all nums
		sum := 0
		for _, num := range nums {
			sum += num
		}
		return sum
	},
	TYPE_ID_PROD: func(nums ...int) int {
		// return the product of all nums
		prod := 1
		for _, num := range nums {
			prod *= num
		}
		return prod
	},
	TYPE_ID_MIN: func(nums ...int) int {
		min := nums[0]
		for _, num := range nums {
			if num < min {
				min = num
			}
		}
		return min
	},
	TYPE_ID_MAX: func(nums ...int) int {
		max := nums[0]
		for _, num := range nums {
			if num > max {
				max = num
			}
		}
		return max
	},
	TYPE_ID_GREATER: func(nums ...int) int {
		if nums[0] > nums[1] {
			return 1
		} else {
			return 0
		}
	},
	TYPE_ID_LESS: func(nums ...int) int {
		if nums[0] < nums[1] {
			return 1
		} else {
			return 0
		}
	},
	TYPE_ID_EQUAL: func(nums ...int) int {
		if nums[0] == nums[1] {
			return 1
		} else {
			return 0
		}
	},
}

// a function that takes an array of bytes, the start position and the
// length of the field and returns the value of the field as an integer
func readField(buff []byte, pos *int, len int) int {
	value := 0
	for i := 0; i < len; i++ {
		value = value << 1
		selectedBit := int(buff[*pos+i])
		value |= selectedBit
	}
	*pos += len
	return value
}

// Parse the variable communication into a hex number and convert the hex number into a binary number
func readBin(communication string) []byte {
	// Convert the hex number into a binary number in an array
	numBits := len(communication) * 4
	binMessage := make([]byte, numBits)
	for i, letter := range communication {
		num, _ := strconv.ParseInt(string(letter), 16, 8)
		for j := 0; j < 4; j++ {
			bitAtPosition := (num >> (3 - j)) & 1
			binMessage[i*4+j] = byte(bitAtPosition)
		}
	}
	return binMessage
}

func parsePackage(buff []byte, pos *int) int {
	println("---------")
	value := 0
	version := readField(buff, pos, 3)
	typeId := readField(buff, pos, 3)
	fmt.Printf("version: %v   typeId: %v\n", version, typeId)
	if typeId == TYPE_ID_LITERAL {
		value = parseLiteral(buff, pos)
		fmt.Printf("literal: %v\n", value)
	} else {
		lengthTypeId := readField(buff, pos, 1)
		valuesList := make([]int, 0)
		fmt.Printf("operatorLengthType: %v\n", lengthTypeId)
		if lengthTypeId == OPERATOR_LENGTH_BITS {
			operatorLength := readField(buff, pos, 15)
			fmt.Printf("operatorLength: %d\n", operatorLength)
			frozenPosition := *pos
			for *pos < frozenPosition+operatorLength {
				v := parsePackage(buff, pos)
				valuesList = append(valuesList, v)
			}
		} else if lengthTypeId == OPERATOR_LENGTH_NUM {
			operatorNum := readField(buff, pos, 11)
			fmt.Printf("operatorNum: %v\n", operatorNum)
			for i := 0; i < operatorNum; i++ {
				v := parsePackage(buff, pos)
				valuesList = append(valuesList, v)
			}
		}
		// apply the operator on the valuesList
		fmt.Printf("valuesList: %v\n", valuesList)
		op := OPERATORS_MAP[typeId]
		value = op(valuesList...)
	}
	return value
}

func parseLiteral(buff []byte, pos *int) int {
	next := 1
	value := 0
	for next == 1 {
		value = value << 4
		next = readField(buff, pos, 1)
		literal := readField(buff, pos, 4)
		value |= literal
	}
	return value
}

func parseOperator(buff []byte, pos *int, op int) int {
	value := 0
	return value
}

func main() {
	binMessage := readBin("C20D718021600ACDC372CD8DE7A057252A49C940239D68978F7970194EA7CCB310088760088803304A0AC1B100721EC298D3307440041CD8B8005D12DFD27CBEEF27D94A4E9B033006A45FE71D665ACC0259C689B1F99679F717003225900465800804E39CE38CE161007E52F1AEF5EE6EC33600BCC29CFFA3D8291006A92CA7E00B4A8F497E16A675EFB6B0058F2D0BD7AE1371DA34E730F66009443C00A566BFDBE643135FEDF321D000C6269EA66545899739ADEAF0EB6C3A200B6F40179DE31CB7B277392FA1C0A95F6E3983A100993801B800021B0722243D00042E0DC7383D332443004E463295176801F29EDDAA853DBB5508802859F2E9D2A9308924F9F31700AA4F39F720C733A669EC7356AC7D8E85C95E123799D4C44C0109C0AF00427E3CC678873F1E633C4020085E60D340109E3196023006040188C910A3A80021B1763FC620004321B4138E52D75A20096E4718D3E50016B19E0BA802325E858762D1802B28AD401A9880310E61041400043E2AC7E8A4800434DB24A384A4019401C92C154B43595B830002BC497ED9CC27CE686A6A43925B8A9CFFE3A9616E5793447004A4BBB749841500B26C5E6E306899C5B4C70924B77EF254B48688041CD004A726ED3FAECBDB2295AEBD984E08E0065C101812E006380126005A80124048CB010D4C03DC900E16A007200B98E00580091EE004B006902004B00410000AF00015933223100688010985116A311803D05E3CC4B300660BC7283C00081CF26491049F3D690E9802739661E00D400010A8B91F2118803310A2F43396699D533005E37E8023311A4BB9961524A4E2C027EC8C6F5952C2528B333FA4AD386C0A56F39C7DB77200C92801019E799E7B96EC6F8B7558C014977BD00480010D89D106240803518E31C4230052C01786F272FF354C8D4D437DF52BC2C300567066550A2A900427E0084C254739FB8E080111E0")
	fmt.Printf("binMessage %v\n", binMessage)
	pos := 0
	versionSum := parsePackage(binMessage, &pos)
	fmt.Printf("result: %v\n", versionSum)
}
