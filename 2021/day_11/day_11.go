package main

import (
	"container/list"
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
	"time"
)

func GetOctopuses(path string) [][]int {
	content, _ := ioutil.ReadFile(path)
	lines := strings.Split(string(content), "\n")
	floorMap := make([][]int, len(lines))
	for i, line := range lines {
		numberLine := make([]int, 0)
		for _, cell := range strings.Split(line, "") {
			num, _ := strconv.Atoi(cell)
			numberLine = append(numberLine, num)
		}
		floorMap[i] = numberLine
	}
	return floorMap
}

type point struct {
	x int
	y int
}

var ADJACENT = []point{
	{0, -1},
	{1, 0},
	{0, 1},
	{-1, 0},
	{-1, -1},
	{1, -1},
	{1, 1},
	{-1, 1},
}

func Main1() {
	energy := GetOctopuses("2021/day_11/day_11_input.txt")
	start := time.Now()
	queue := list.New()

	flashes := 0
	// iterate 100 times

	for step := 0; step < 100; step++ {
		// increment all
		for i, line := range energy {
			for j := range line {
				line[j] = (line[j] + 1) % 10
				if line[j] == 0 {
					queue.PushBack(point{i, j})
					flashes++
				}
			}
		}
		for queue.Len() > 0 {
			current := queue.Front().Value.(point)
			queue.Remove(queue.Front())
			for _, p := range ADJACENT {
				i := current.x + p.x
				j := current.y + p.y
				// if the point is in bounds
				if i >= 0 && i < len(energy) && j >= 0 && j < len(energy[0]) && energy[i][j] != 0 {
					// increase the value of the point
					energy[i][j] = (energy[i][j] + 1) % 10
					if energy[i][j] == 0 {
						queue.PushBack(point{i, j})
						flashes++
					}
				}

			}
		}
	}
	elapsed := time.Since(start)
	// not 1637
	fmt.Printf("day 11.1: %v,   %s\n", flashes, elapsed)
}

func Main2() {
	energy := GetOctopuses("2021/day_11/day_11_input.txt")
	start := time.Now()
	queue := list.New()
	steps := 0
	synchronized := false
	maxFlashes := len(energy) * len(energy[0])
	// iterate until synchronized
	for !synchronized {
		flashes := 0
		steps++
		// increment all
		for i, line := range energy {
			for j := range line {
				line[j] = (line[j] + 1) % 10
				if line[j] == 0 {
					queue.PushBack(point{i, j})
					flashes++
				}
			}
		}
		for queue.Len() > 0 {
			current := queue.Front().Value.(point)
			queue.Remove(queue.Front())
			for _, p := range ADJACENT {
				i := current.x + p.x
				j := current.y + p.y
				// if the point is in bounds
				if i >= 0 && i < len(energy) && j >= 0 && j < len(energy[0]) && energy[i][j] != 0 {
					// increase the value of the point
					energy[i][j] = (energy[i][j] + 1) % 10
					if energy[i][j] == 0 {
						queue.PushBack(point{i, j})
						flashes++
					}
				}

			}
		}
		if flashes == maxFlashes {
			synchronized = true
		}

	}
	elapsed := time.Since(start)
	fmt.Printf("day 11.2: %v,   %s\n", steps, elapsed)
}

func main() {
	Main1()
	Main2()
}
