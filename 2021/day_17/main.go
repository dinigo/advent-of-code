package main

import (
	"fmt"
	"regexp"
	"strconv"
)

func parseRange(input string) (int, int, int, int) {
	r := regexp.MustCompile(`x=(-?\d+)\.{2}(-?\d+),\sy=(-?\d+)\.\.(-?\d+)`)
	matches := r.FindStringSubmatch(input)
	minX, _ := strconv.Atoi(matches[1])
	maxX, _ := strconv.Atoi(matches[2])
	minY, _ := strconv.Atoi(matches[3])
	maxY, _ := strconv.Atoi(matches[4])
	return minX, maxX, minY, maxY
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func main() {
	fmt.Println("Hello, World!")
	inputString := "target area: x=94..151, y=-156..-103"
	minX, maxX, minY, maxY := parseRange(inputString)
	fmt.Println(minX, maxX, maxY, minY)
	ans := 0
	count := 0
	for DX := 0; DX <= 1000; DX++ {
		for DY := -1000; DY <= 1000; DY++ {
			// initialization
			highest_y := 0
			ok := false
			x := 0
			y := 0
			dx := DX
			dy := DY
			// for each initial speed
			for t := 0; t < 1000; t++ {
				x += dx
				y += dy
				highest_y = max(highest_y, y)
				if dx > 0 {
					dx--
				}
				dy--
				if x >= minX && x <= maxX && y >= minY && y <= maxY {
					fmt.Printf("initial vector (%d, %d) reached target\n", DX, DY)
					ans = max(ans, highest_y)
					ok = true
				}
			}
			if ok {
				ans = max(ans, highest_y)
				fmt.Printf("(%d,%d)\n", DX, DY)
				count++
			}
		}
	}
	fmt.Printf("ans: %d\ncount:%d", ans, count)
}
