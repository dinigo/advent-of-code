package main

import (
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
)

func Main1(filePath string) {
	// read file and initialize matrix
	dat, _ := os.ReadFile(filePath)
	fileStr := string(dat)
	lines := strings.Split(fileStr, "\n")
	numBits := len(lines[0])
	binSum := make([]int, numBits)
	for _, line := range lines {
		for j, char := range line {
			binNum, _ := strconv.Atoi(string(char))
			binSum[j] += binNum
		}
	}
	gammaList := make([]int, numBits)
	for i, b := range binSum {
		if b > len(lines)/2 {
			gammaList[i] = 1
		}
	}
	gammaRate := 0
	for i := range gammaList {
		factor := int(math.Pow(2, float64(i)))
		dec := factor * gammaList[numBits-1-i]
		gammaRate += dec
	}
	maxNum := uint64(math.Pow(2, float64(numBits))) - 1
	epsilonRate := (^uint64(gammaRate)) & maxNum
	fmt.Printf("day 3.1: %v\n", gammaRate*int(epsilonRate))
}

func BinArrayToInt(binArray []int) int {
	sum := 0
	for i, n := range binArray {
		sum += int(math.Pow(2, float64(len(binArray)-1-i))) * n
	}
	return sum
}

func FileToNums(filePath string) ([][]int, int) {
	dat, _ := os.ReadFile(filePath)
	fileStr := string(dat)
	lines := strings.Split(fileStr, "\n")
	numBits := len(lines[0])
	numberMatrix := make([][]int, len(lines))
	for i, line := range lines {
		numberMatrix[i] = make([]int, numBits)
		for j, char := range line {
			binNum, _ := strconv.Atoi(string(char))
			numberMatrix[i][j] = binNum
		}
	}
	return numberMatrix, numBits
}

func LifeSupportRating(nums [][]int, pos int, numBits int, comparator func(int, int) bool) int {
	if len(nums) == 1 {
		return BinArrayToInt(nums[0])
	}

	sum := 0
	for _, line := range nums {
		sum += line[pos]
	}
	filterNum := 0
	if comparator(sum, len(nums)) {
		filterNum = 1
	}
	var filteredNums [][]int
	for _, line := range nums {
		if line[pos] == filterNum {
			filteredNums = append(filteredNums, line)
		}
	}
	return LifeSupportRating(filteredNums, pos+1, numBits, comparator)

}

func compareOxygen(sum int, remaining int) bool {
	return sum >= (remaining/2 + remaining%2)
}
func compareCo2(sum int, remaining int) bool {
	return sum < (remaining/2 + remaining%2)
}

func Main2() {
	numberMatrix, numBits := FileToNums("2021/day_3/day_3_input.txt")
	oxygen := LifeSupportRating(numberMatrix, 0, numBits, compareOxygen)
	co2 := LifeSupportRating(numberMatrix, 0, numBits, compareCo2)
	fmt.Printf("day 3.2: %v\n", co2*oxygen)
}

func main() {
	Main1("2021/day_3/day_3_input.txt")
	Main2()
}
