package main

import (
	"fmt"
	"io/ioutil"
	"sort"
	"strconv"
	"strings"
)

func GetMap(path string) [][]int {
	content, _ := ioutil.ReadFile(path)
	lines := strings.Split(string(content), "\n")
	floorMap := make([][]int, len(lines))
	for i, line := range lines {
		numberLine := make([]int, 0)
		for _, cell := range strings.Split(line, "") {
			num, _ := strconv.Atoi(cell)
			numberLine = append(numberLine, num)
		}
		floorMap[i] = numberLine
	}
	return floorMap
}

func Main1() {
	floorMap := GetMap("2021/day_9/day_9_input.txt")
	// find the low points
	risk := 0
	for i, line := range floorMap {
		for j, cell := range line {

			isNorthOk := i == 0 || cell < floorMap[i-1][j]
			isSouthOk := i == len(floorMap)-1 || cell < floorMap[i+1][j]
			isWestOk := j == 0 || cell < line[j-1]
			isEastOk := j == len(line)-1 || cell < line[j+1]
			if isNorthOk && isSouthOk && isWestOk && isEastOk {
				risk += 1 + cell
			}
		}
	}
	fmt.Printf("day 9.1: %v\n", risk)
}

type point struct {
	x int
	y int
}

func Main2() {
	floorMap := GetMap("2021/day_9/day_9_input.txt")
	visitedPoints := make(map[point]bool)
	basins := make([]int, 0)
	for i, line := range floorMap {
		for j, cell := range line {
			_, visited := visitedPoints[point{x: i, y: j}]
			if !visited && cell != 9 {
				// get the size of each basin starting from each point if it wasn't visited before
				basinSize := SearchBasins(floorMap, i, j, visitedPoints)
				basins = append(basins, basinSize)
			}
		}
	}
	// sort descending
	sort.Slice(basins, func(i, j int) bool {
		return basins[i] > basins[j]
	})
	fmt.Printf("day 9.2: %v\n", basins[0]*basins[1]*basins[2])

}

func SearchBasins(floorMap [][]int, i int, j int, visited map[point]bool) int {
	// check if we've been here
	currentPoint := point{x: i, y: j}
	if _, alreadyVisited := visited[currentPoint]; alreadyVisited {
		return 0
	}
	visited[currentPoint] = true
	if floorMap[i][j] == 9 {
		return 0
	}
	sum := 1

	if i > 0 {
		sum += SearchBasins(floorMap, i-1, j, visited)
	}
	if i < len(floorMap)-1 {
		sum += SearchBasins(floorMap, i+1, j, visited)
	}
	if j > 0 {
		sum += SearchBasins(floorMap, i, j-1, visited)
	}
	if j < len(floorMap[i])-1 {
		sum += SearchBasins(floorMap, i, j+1, visited)
	}
	return sum
}

func main() {
	Main1()
	Main2()
}
