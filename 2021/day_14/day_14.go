package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"strings"
)

type pair struct{ a, b byte }

func GetPolimers(path string) (string, map[pair]byte) {
	// read the file
	content, _ := ioutil.ReadFile(path)
	lines := strings.Split(string(content), "\n")
	// get all coordinates
	chain := lines[0]
	book := make(map[pair]byte)
	for _, line := range lines[2:] {
		book[pair{line[0], line[1]}] = line[6]
	}
	return chain, book
}

func main() {
	chain, book := GetPolimers("2021/day_14/day_14_input.txt")
	fmt.Printf("%v, %v\n", book, chain)

	iterations := 6
	for i := 0; i < iterations; i++ {
		println("step ", i)
		auxChain := ""
		for j := 0; j < len(chain)-1; j++ {
			auxChain += string(chain[j])
			reaction, ok := book[pair{chain[j], chain[j+1]}]
			if ok {
				auxChain += string(reaction)
			}
		}
		auxChain += string(chain[len(chain)-1])
		chain = auxChain
		fmt.Printf("%d, %v\n", i+1, chain)
	}
	// sum them up
	summary := map[rune]int{}
	for _, char := range chain {
		summary[char]++
	}
	// get biggest and smallest
	biggest := 0
	smallest := math.MaxInt
	for _, count := range summary {
		if count > biggest {
			biggest = count
		}
		if count < smallest {
			smallest = count
		}
	}
	fmt.Printf("day 14.1: %d\n", biggest-smallest)
}
