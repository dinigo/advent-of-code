package main

import (
	"fmt"
	"io/ioutil"
	"regexp"
	"strconv"
	"strings"
)

type fold struct {
	dimension string
	index     int
}

type point struct {
	x int
	y int
}

func main() {
	input, _ := ioutil.ReadFile("2021/day_13/day_13_input.txt")
	inputString := string(input)
	// read pairs of numbers in each line of input string separated by coma into a [][]int
	points := make([]point, 0)
	folds := make([]fold, 0)
	startReadingFods := false
	maxX := 0
	maxY := 0
	for _, line := range strings.Split(inputString, "\n") {
		if line == "" {
			startReadingFods = true
			continue
		}
		if startReadingFods {
			matches := regexp.MustCompile(`fold along (\w)=(\d+)`).FindAllStringSubmatch(line, -1)
			index, _ := strconv.Atoi(matches[0][2])
			folds = append(folds, fold{matches[0][1], index})
		} else {
			pair := strings.Split(line, ",")
			x, _ := strconv.Atoi(pair[0])
			y, _ := strconv.Atoi(pair[1])
			points = append(points, point{x, y})
			if x > maxX {
				maxX = x
			}
			if y > maxY {
				maxY = y
			}
		}
	}
	// create the display
	display := make([][]bool, maxY+1)
	for i := range display {
		display[i] = make([]bool, maxX+1)
	}
	// apply the dots
	for _, point := range points {
		display[point.y][point.x] = true
	}

	switch folds[0].dimension {
	case "x":
		display = FoldX(display, folds[0].index)
	case "y":
		display = FoldY(display, folds[0].index)
	}
	sumDots := 0
	for _, line := range display {
		for _, dot := range line {
			if dot {
				sumDots++
			}
		}
	}

	fmt.Printf("day 13.1: %v\n", sumDots)
	for _, fold := range folds[1:] {
		switch fold.dimension {
		case "x":
			display = FoldX(display, fold.index)
		case "y":
			display = FoldY(display, fold.index)
		}
	}
	fmt.Printf("day 13.2: %v\n", sumDots)
	PrintMatrix(display)
}

func PrintMatrix(matrix [][]bool) {
	println("\nDISPLAY!")
	for _, row := range matrix {
		for _, cell := range row {
			char := " "
			if cell {
				char = "#"
			}
			fmt.Printf("%s", char)
		}
		fmt.Printf("\n")
	}
}

func FoldY(matrix [][]bool, fold int) [][]bool {
	for i := 0; i < fold; i++ {
		for j := 0; j < len(matrix[0]); j++ {
			matrix[i][j] = matrix[len(matrix)-1-i][j] || matrix[i][j]
		}
	}
	return matrix[:fold]
}

func FoldX(matrix [][]bool, fold int) [][]bool {
	for i := 0; i < len(matrix); i++ {
		for j := 0; j < fold; j++ {
			matrix[i][j] = matrix[i][len(matrix[i])-1-j] || matrix[i][j]
		}
		matrix[i] = matrix[i][:fold]
	}
	return matrix
}
