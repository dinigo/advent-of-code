package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

type command struct {
	name   string
	amount int
}

func ReadCommandsFileWithAim(filePath string) []command {
	dat, _ := os.ReadFile(filePath)
	commandsStr := string(dat)
	lines := strings.Split(commandsStr, "\n")
	commands := make([]command, len(lines))

	for i, line := range lines {
		splitCommand := strings.Split(line, " ")
		commandAmount, _ := strconv.Atoi(splitCommand[1])
		commands[i] = command{
			name:   splitCommand[0],
			amount: commandAmount,
		}
	}
	return commands
}

func Main1() {
	commands := ReadCommandsFileWithAim("2021/day_2/day_2_input.txt")
	deep := 0
	far := 0
	for _, command := range commands {
		switch command.name {
		case "forward":
			far += command.amount
		case "down":
			deep += command.amount
		case "up":
			deep -= command.amount
		}
	}
	fmt.Printf("day 2.1: %d\n", deep*far)
}

func Main2() {
	commands := ReadCommandsFileWithAim("2021/day_2/day_2_input.txt")
	deep := 0
	far := 0
	aim := 0
	for _, command := range commands {
		switch command.name {
		case "forward":
			far += command.amount
			deep += aim * command.amount
		case "down":
			aim += command.amount
		case "up":
			aim -= command.amount
		}
	}
	fmt.Printf("day 2.2: %d\n", deep*far)

}

func main() {
	Main1()
	Main2()
}
