package main

import (
	"fmt"
	"io/ioutil"
	"regexp"
	"strconv"
	"strings"
)

var INPUT_DIGITS = 10
var OUTPUT_DIGITS = 4
var DIGITS_PER_LINE = INPUT_DIGITS + OUTPUT_DIGITS

func Main2() {
	content, _ := ioutil.ReadFile("2021/day_8/day_8_input.txt")
	digitsRaw := regexp.MustCompile(`\w+`).FindAllString(string(content), -1)

	numPuzzles := len(digitsRaw) / DIGITS_PER_LINE
	inputs := make([][]string, numPuzzles)  // 14 digits per line
	outputs := make([][]string, numPuzzles) // 14 digits per line
	for i, j := 0, 0; j < len(digitsRaw); i, j = i+1, j+DIGITS_PER_LINE {
		inputs[i] = digitsRaw[j : j+INPUT_DIGITS]
		outputs[i] = digitsRaw[j+INPUT_DIGITS : j+DIGITS_PER_LINE]
	}

	sum := 0
	for i := 0; i < numPuzzles; i++ {
		sum += GetOutputNumber(inputs[i], outputs[i])
	}

	fmt.Printf("day 8.2: %v ", sum)

}

func GetOutputNumber(input []string, output []string) int {
	digits := make(map[int]byte)
	// assign easy ones
	for _, digit := range input {
		binDigit := CharToBin(digit)
		switch len(digit) {
		case 2:
			digits[1] = binDigit
		case 3:
			digits[7] = binDigit
		case 4:
			digits[4] = binDigit
		case 7:
			digits[8] = binDigit
		}
	}
	// assign the rest
	for _, digit := range input {
		binDigit := CharToBin(digit)
		switch len(digit) {
		case 5:
			if digits[1]&binDigit == digits[1] {
				digits[3] = binDigit
			} else if digits[4]|binDigit == digits[8] {
				digits[2] = binDigit
			} else {
				digits[5] = binDigit
			}
		case 6:
			if digits[1]|binDigit == digits[8] {
				digits[6] = binDigit
			} else if digits[4]|binDigit == digits[8] {
				digits[0] = binDigit
			} else {
				digits[9] = binDigit
			}
		}
	}
	reverseDigits := make(map[byte]string)
	for num, bin := range digits {
		reverseDigits[bin] = strconv.Itoa(num)
	}
	// check the numbers in output
	outputStr := ""
	for _, digit := range output {
		binDigit := CharToBin(digit)
		outputStr += reverseDigits[binDigit]
	}
	outputNum, _ := strconv.Atoi(outputStr)
	return outputNum
}

func CharToBin(chars string) byte {
	result := 0
	for _, char := range chars {
		result |= 1 << byte(char-'a')
	}
	return byte(result)
}

func Main1() {
	content, _ := ioutil.ReadFile("2021/day_8/day_8_input.txt")
	count := 0
	for _, line := range strings.Split(string(content), "\n") {
		digits := regexp.MustCompile(`\w+`).FindAllString(line, -1)
		outputs := digits[10:]
		for _, digit := range outputs {
			if len(digit) == 2 || len(digit) == 3 || len(digit) == 4 || len(digit) == 7 {
				count++
			}
		}
	}
	fmt.Printf("day 8.1: %v\n", count)
}

func main() {
	Main1()
	Main2()
}
