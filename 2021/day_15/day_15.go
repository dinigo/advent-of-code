package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"strings"
	"time"
)

func GetNumberMatrix(path string) [][]int {
	content, _ := ioutil.ReadFile(path)
	lines := strings.Split(string(content), "\n")
	matrix := make([][]int, len(lines))
	for i, line := range lines {
		numberLine := make([]int, 0)
		for _, char := range line {
			numberLine = append(numberLine, int(char-'0'))
		}
		matrix[i] = numberLine
	}
	return matrix
}

var ADJACENT = [][]int{{0, -1}, {1, 0}, {0, 1}, {-1, 0}}

type Node struct {
	x, y int
}

func tiledMatrix(matrix [][]int, tilesLeft int, tilesDown int) [][]int {
	//read matrix into risk array
	rows := len(matrix)
	cols := len(matrix[0])
	auxMatrix := make([][]int, rows*tilesDown)
	for i := range auxMatrix {
		auxMatrix[i] = make([]int, cols*tilesLeft)
	}
	// initialize auxMatrix
	for i := range auxMatrix {
		for j := range auxMatrix[i] {
			tileNumberX := int(math.Floor(float64(j / rows)))
			tileNumberY := int(math.Floor(float64(i / rows)))
			newVal := matrix[i%rows][j%cols] + tileNumberX + tileNumberY
			moduleVal := (newVal-1)%9 + 1
			auxMatrix[i][j] = moduleVal
		}
	}
	return auxMatrix
}

func Main2() {
	tile := GetNumberMatrix("2021/day_15/day_15_input.txt")
	matrix := tiledMatrix(tile, 5, 5)
	start := time.Now()
	cost := DijkstraTheHellOutOfThisMatrix(matrix, Node{x: 0, y: 0}, Node{x: len(matrix) - 1, y: len(matrix[0]) - 1})
	elapsed := time.Since(start)
	fmt.Printf("day 15.2: %v , %s\n", cost, elapsed)
}

func Main1() {
	matrix := GetNumberMatrix("2021/day_15/day_15_input.txt")
	start := time.Now()
	cost := DijkstraTheHellOutOfThisMatrix(matrix, Node{x: 0, y: 0}, Node{x: len(matrix) - 1, y: len(matrix[0]) - 1})
	elapsed := time.Since(start)

	fmt.Printf("day 15.1: %v , %s\n", cost, elapsed)
}

// function to pop min cost node from border
func getMinCostNode(border map[Node]int) Node {
	// sort border by ascending cost
	minCost := math.MaxInt
	var minNode Node
	for node, cost := range border {
		if cost < minCost {
			minCost = cost
			minNode = node
		}
	}
	// pop min cost node
	return minNode
}

func DijkstraTheHellOutOfThisMatrix(matrix [][]int, start Node, end Node) int {
	// list of nodes to visit with cost set to infinite
	visited := map[Node]bool{}
	border := map[Node]int{start: 0}
	// should be sorted by cost
	solution := make([][]int, len(matrix))
	for i := range solution {
		solution[i] = make([]int, len(matrix[i]))
	}

	for len(border) > 0 {
		// get the node with the lowest cost from the border
		minCostNode := getMinCostNode(border)
		// get a list of its adjacent nodes with the cost to get there
		solution[minCostNode.y][minCostNode.x] = border[minCostNode]
		// add this min cost node to the visited nodes
		visited[minCostNode] = true
		// add all adjacent nodes to the border to be visited later
		addCurrentNodeNeighbours(minCostNode, matrix, visited, border)
		// delete from the border so we don't pick it again
		delete(border, minCostNode)
	}
	return solution[end.y][end.x]
}

// function to get not yet visited neighbors of a node with its corresponding cost
func addCurrentNodeNeighbours(current Node, matrix [][]int, visited map[Node]bool, border map[Node]int) {
	for _, p := range ADJACENT {
		i := current.y + p[1]
		j := current.x + p[0]
		// if the point is in bounds
		isWithinBounds := i >= 0 && i < len(matrix) && j >= 0 && j < len(matrix[0]) && matrix[i][j] != 0
		_, isVisited := visited[Node{x: j, y: i}]
		_, isBorder := border[Node{x: j, y: i}]
		if isWithinBounds && !isVisited && !isBorder {
			border[Node{x: j, y: i}] = border[current] + matrix[i][j]
		}
	}
}

func main() {
	Main1()
	Main2()
}
