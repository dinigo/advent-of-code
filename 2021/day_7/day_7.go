package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"strconv"
	"strings"
)

func getError(crabPositions []int, errFunction func(int, int) int) int {

	// get the range of positions to iterate
	furthestCrab := 0
	for _, crab := range crabPositions {
		if crab > furthestCrab {
			furthestCrab = crab
		}
	}
	// get min
	err := math.MaxInt
	for i := 0; i <= furthestCrab; i++ {
		currErr := 0
		for _, crab := range crabPositions {
			currErr += errFunction(i, crab)
			if currErr > err {
				break
			}
		}
		if currErr < err {
			err = currErr
		}
	}
	return err
}

func linearErrFunc(pos int, crab int) int {
	return int(math.Abs(float64(pos - crab)))
}

func incrementalErrFunc(pos int, crab int) int {
	sum := 0
	final := int(math.Abs(float64(pos - crab)))
	for i := 1; i <= final; i++ {
		sum += i
	}
	return sum
}

func main() {
	content, _ := ioutil.ReadFile("2021/day_7/day_7_input.txt")
	crabPositionsStr := strings.Split(string(content), ",")
	crabPositions := make([]int, len(crabPositionsStr))
	for i, fd := range crabPositionsStr {
		num, _ := strconv.Atoi(fd)
		crabPositions[i] = num
	}

	errLinear := getError(crabPositions, linearErrFunc)
	fmt.Printf("day 5.1: fuel %v\n", errLinear)
	errNonLinear := getError(crabPositions, incrementalErrFunc)
	fmt.Printf("day 5.1: fuel %v\n", errNonLinear)
}
