// perform a single step in the binary search. Used by the "reduce" function
function bin_search([min, max],curr){
		const pivot = ~~((max-min)/2) + min;
		return curr? [pivot+1, max] : [min, pivot]
}

// get a single seat ID
function get_seat_id(boarding){
    const dirs = boarding.split('').map(el => ['B','R'].includes(el));
    const row = dirs.slice(0,7).reduce(bin_search, [0, 127])[0];
    const col = dirs.slice(7).reduce(bin_search, [0, 7])[0];
    return row * 8 + col;
}

function main(problem_input){
    const all_boards = problem_input.split('\n');
    const all_seat_id = all_boards.map(get_seat_id);
    return Math.max(...all_seat_id);
}
