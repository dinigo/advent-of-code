function main(sequence, preamble_len) {
    var preamble = [];
    for (let i = 0; i < sequence.length; i++) {
        if (preamble.length === preamble_len) {
            var found = false;
            for (let k = 0; k < preamble.length - 1 && !found; k++) {
                for (let j = k + 1; j < preamble.length && !found; j++) {
                    if (sequence[i] === preamble[k] + preamble[j]) {
                        found = true;
                    }
                }
            }
            if (!found) return sequence[i];
            preamble.shift();
        }
        preamble.push(sequence[i]);
    }
}
