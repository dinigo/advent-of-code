const tree = input
    .split(/\n/)
    .map(line => /(?<parent>\w+\s\w+) bags contain (?<content>.*)./.exec(line).groups)
    .map(({parent, content}) => ({
        parent,
        children: content != 'no other bags' && content
            .split(', ')
            .map(c => /(?<num>\d+) (?<child>\w+\s\w+) bags?/.exec(c).groups)
    }))
	.reduce((acum,{parent, children}) => ({...acum, [parent]: children}), {})

function sum_bags(tree, curr){
	return (children = tree[curr])? children.reduce((acum, {num, child}) => acum + (+num) * (1 + sum_bags(tree, child)), 0) : 0; 
}

sum_bags(tree, 'shiny gold')
