const main = input => input
    .split(/\n{2,}/)
    .map(g => Object.entries(
            g
                .split(/\n?/)
                .reduce((acum,cur) => ({...acum, [cur]: (acum[cur] || 0)+1}),{})
		)
        .filter(([_, t]) => t == (g.match(/\n/g)||[]).length + 1)
    )
    .flat()
    .length;
