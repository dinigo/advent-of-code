function main(input){
	let accumulator = 0;
	let pc=0;
    const program = input.split('\n').map(l => l.split(' '));
	const visited = [];
	const operations = {
		acc: v => {accumulator+=v; pc++},
		jmp: j => {pc+=j},
		nop: _ => {pc++},
	};
	while(!visited.includes(pc)){
		visited.push(pc);
		const [op, v] = program[pc];
		operations[op](+v);
	}
	return accumulator;
}
