// mandatory fields with their validators
const transforms = {
    byr: v =>  ([y] = v.match(/^\d{4}$/) || [0]) && +y >= 1920 && +y <= 2002,
    iyr: v =>  ([y] = v.match(/^\d{4}$/) || [0]) && +y >= 1920 && +y <= 2020,
    eyr: v =>  ([y] = v.match(/^\d{4}$/) || [0]) && +y >= 2020 && +y <= 2030,
    hgt: v => ([_, h, m] = v.match(/^(\d+)(cm|in)$/) || [0, 0, '']) && ((m == 'cm' && +h >= 150 && +h <= 193) || (m == 'in' && +h >= 59 && +h <= 76)),
    hcl: v => /^#[0-9a-f]{6}$/.test(v),
    ecl: v => ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'].includes(v),
    pid: v => /^\d{9}$/.test(v)
};

// mandatory fields list
const transform_keys = Object.keys(transforms).sort().toString();

// parse and validate function
const parse_passports = t => t
	.split(/\n\n/) // create a list of passports
	.map(p => p
		.split(/\s|\n/)  // separate fields on each passport
		.filter(el => !el.startsWith('cid:'))  // remove optional field
		.map(el => el.split(':'))
  	    .reduce((acum,[key, val]) => Object.assign(acum, {[key]: val}), {})  // build and object out of the passport fields
	)
	.filter(el => Object.keys(el).sort() == transform_keys)  // keep only passports with all the mandatory fields
	.map(p => Object.entries(p)
		.every(([key, val]) => transforms[key](val))  // if every validation passes the passport is set to true
	)
	.filter(el => el)  // filter only true elements (passports with all checks good)
	.length;  // get how many passed
