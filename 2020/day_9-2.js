function main(sequence, target) {
    for(let i=0; i<sequence.indexOf(target)-1; i++){
        const set = [sequence[i]];
        let sum = sequence[i];
        let too_big = false;
        for(let j=i+1; j<sequence.indexOf(target) && !too_big; j++){
            set.push(sequence[j])
            sum += sequence[j];
            if(sum === target) return Math.min(...set) + Math.max(...set);
            else if(sum > target) too_big = true;
        }
    }
}
