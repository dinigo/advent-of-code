function main(adapters) {
    const diff = adapters
        .split('\n').map(el => +el)
        .sort((a, b) => a - b)
        .map((v, i, a) => v - (a[i - 1] || 0));
    const ones = diff.filter(el => el === 1).length;
    const threes = diff.filter(el => el === 3).length + 1;
    return ones * threes;
}
