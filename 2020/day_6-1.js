const main = input => input
    .split('\n\n')
    .map(g => g
        .split('\n')
        .map(p => p.split(''))
        .reduce((acum, p), {})
        .split(' ')
    )
    .reduce((a,c) => a+c);
