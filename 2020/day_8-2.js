const program = input.split('\n').map(l => l.split(' '));

function run(program){
	let accumulator = 0;
	let pc=0;
	const visited = [];
	const operations = {
		acc: v => {accumulator+=v; pc++},
		jmp: j => {pc+=j},
		nop: _ => {pc++},
	};
	while(!visited.includes(pc)){
		visited.push(pc);
		const [op, v] = program[pc];
		operations[op](+v);
		if(pc === program.length) return accumulator;
	}
	return false;
}

let result = false;
for(let i = 0; i < program.length; i++){
	const [op] = program[i];
	if(op !== 'acc'){
		const switched = op === 'jmp'? 'nop' : 'jmp';
		program[i][0] = switched;
		result = run(program);
		if(result) break;
		program[i][0] = op;
	}
}
console.log(result);
