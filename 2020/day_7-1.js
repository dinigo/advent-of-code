function main (input){
    const relations = input
        .split(/\n/)
        .map(line => /(?<parent>\w+\s\w+) bags contain (?<content>.*)./.exec(line).groups)
        .map(({parent, content}) => ({
            parent,
            children: content != 'no other bags' && content
                .split(', ')
                .map(c => c.match(/\d+ (\w+\s\w+) bags?/)[1])
        }))
        .flatMap(({parent, children}) => (children || [false]).map(child => ({child, parent})))

    const roots = relations.filter(n => !n.child).map(n => n.parent)

    function count_nodes(relations, target, current, flag){
        var parents = relations
            .filter(n => current == n.child)
            .map(n => n.parent)
            .filter((v,i,a) => a.indexOf(v) == i);
        //console.log(`iteration:   ${current} : ${parents.join(',')}\t -- ${flag}`);
        if(parents.length){
            return parents
                .flatMap(parent => count_nodes(relations, target, parent, current == target? true : flag))
                .concat(flag && current)
                .filter((v,i,a) => v && a.indexOf(v) == i);
        }
        else if(flag) return current;
    }
    var res = roots
        .flatMap(root => count_nodes(relations, 'shiny gold', root, false))
        .filter((v,i,a) => a.indexOf(v) == i)
        .length;

    return res;
}
