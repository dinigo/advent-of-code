function main(grid_text) {
    // parse the matrix
    let grid = grid_text.split('\n').map(el => el.split(''));
    // util to move values instead of refferences
    const deep_copy = o => JSON.parse(JSON.stringify(o));
    // rules table for each type of cell
    const rules = {
        '.': _ => '.',
        'L': adjacent => adjacent.includes('#') ? 'L' : '#',
        '#': adjacent => adjacent.filter(seat => seat === '#').length > 3 ? 'L' : '#'
    };
    // flag to wait for the situation to be stable
    let change;
    // while there are changes iterate
    do {
        change = false;
        // generate an aux grid
        const aux_grid = deep_copy(grid)
        // for each cell calculate the new and put it in the aux
        for (let i = 0; i < grid.length; i++) {
            for (let j = 0; j < grid[0].length; j++) {
                // check adjacent cells
                const adjacent = []
                for (let x = i - 1; x < i + 2; x++) {
                    for (let y = j - 1; y < j + 2; y++) {
                        if (!(x === i && y === j) && grid?.[x]?.[y]) adjacent.push(grid[x][y]);
                    }
                }
                // evaluate adjacent
                const decided = rules[grid[i][j]](adjacent);
                if (decided !== grid[i][j]) {
                    change = true;
                    aux_grid[i][j] = decided;
                }
            }
        }
        // copy the aux in the current to re-iterate
        grid = deep_copy(aux_grid);
    } while (change);
    // count the empty seats
    return grid.flat().filter( el => el === '#').length;
}
