function main(input) {
    const adapters = input
        .split('\n')
        .map(el => +el)
        .sort((a, b) => a - b)
    adapters.unshift(0)
    adapters.push(adapters[adapters.length - 1] + 3)
    console.log(adapters.join(','))
    return count_options_old(adapters, 0, {})
}

function count_options_old(adapters, curr, cache) {
    if (curr === adapters.length - 1) return 1;
    else {
        let acum = 0;
        for (let i = curr + 1; i < curr + 4 && i < adapters.length; i++) {
            if (adapters[i] - adapters[curr] < 4) {
                // use memoization powers!
                if (!(i in cache)) {
                    cache [i] = count_options_old(adapters, i, cache);
                }
                acum += cache[i]
            }
        }
        return acum;
    }
}
