#!/bin/bash

AOC_DAY=${1}
AOC_PART=${2}
AOC_MODE=${3:-test}

WORKFLOW_FILE="day_${AOC_DAY}/part_${AOC_PART}.workflow.yaml"
INPUT_FILE="day_${AOC_DAY}/${AOC_MODE}_data.yaml"
uv run workflows-emulator \
  --source $WORKFLOW_FILE \
  --loglevel ALERT \
  run --data $INPUT_FILE
